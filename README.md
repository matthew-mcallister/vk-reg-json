# vkreg-py

This module provides an interface to process the Vulkan XML registry
suitable for use by code generators.
